package com.demo.mediguide.data;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.demo.mediguide.model.Result;

/**
 * Created by q4J1X056 on 01-12-2015.
 */
public class Drug {

    // Drug Table
    public static final String TABLE_DRUG = "drug";
    // Drug Table Columns
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_MFID = "mfId";
    public static final String COLUMN_AVAILABLE = "available";
    public static final String COLUMN_LABEL = "label";
    public static final String COLUMN_MANUFACTURER = "manufacturer";
    public static final String COLUMN_FORM = "form";
    public static final String COLUMN_HKP_DRUG_CODE = "hkpDrugCode";
    public static final String COLUMN_U_PRICE = "uPrice";
    public static final String COLUMN_PACK_FORM = "packForm";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_UIP="uip";
    public static final String COLUMN_GENERICS = "generics";
    public static final String COLUMN_PRODUCTS_FOR_BRANDS = "productsForBrand";
    public static final String COLUMN_DISCOUNT_PERC="discountPerc";
    public static final String COLUMN_TYPE="type";
    public static final String COLUMN_SLUG="slug";
    public static final String COLUMN_MRP="mrp";
    public static final String COLUMN_IMG_URL="imgURL";
    public static final String COLUMN_P_FORM="pForm";
    public static final String COLUMN_O_PRICE="oPrice";
    public static final String COLUMN_SU="su";
    public static final String COLUMN_PACK_SIZE="packSize";


    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_DRUG
            + "("
            + COLUMN_ID + " integer primary key , "
            + COLUMN_MFID + " integer not null, "
            + COLUMN_AVAILABLE + " integer default 0, "
            + COLUMN_LABEL + " text not null, "
            + COLUMN_MANUFACTURER + " text not null, "
            + COLUMN_FORM + " text not null,"
            + COLUMN_HKP_DRUG_CODE + " integer not null, "
            + COLUMN_U_PRICE + " real not null, "
            + COLUMN_PACK_FORM + " text not null, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_UIP + " integer not null, "
            + COLUMN_GENERICS + " text null, "
            + COLUMN_PRODUCTS_FOR_BRANDS + " text null, "
            + COLUMN_DISCOUNT_PERC + " integer default 0, "
            + COLUMN_TYPE + " text not null, "
            + COLUMN_SLUG + " text not null, "
            + COLUMN_MRP + " real not null, "
            + COLUMN_IMG_URL + " text null, "
            + COLUMN_P_FORM + " text  not null, "
            + COLUMN_O_PRICE + " real not null, "
            + COLUMN_SU + " integer not null, "
            + COLUMN_PACK_SIZE + " text not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(Drug.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_DRUG);
        onCreate(database);
    }


    public static ContentValues getContentValuesFromModel(Result drug)
    {
        ContentValues cv=new ContentValues();
        cv.put(COLUMN_ID,drug.id);
        cv.put(COLUMN_MFID,drug.mfId);
        cv.put(COLUMN_AVAILABLE,drug.available);
        cv.put(COLUMN_LABEL,drug.label);

        cv.put(COLUMN_MANUFACTURER,drug.manufacturer);
        cv.put(COLUMN_FORM,drug.form);
        cv.put(COLUMN_HKP_DRUG_CODE,drug.hkpDrugCode);
        cv.put(COLUMN_U_PRICE,drug.uPrice);

        cv.put(COLUMN_PACK_FORM,drug.packForm);
        cv.put(COLUMN_NAME,drug.name);
        cv.put(COLUMN_UIP,drug.uip);
        cv.put(COLUMN_GENERICS,drug.generics);

        cv.put(COLUMN_PRODUCTS_FOR_BRANDS,drug.productsForBrand);
        cv.put(COLUMN_DISCOUNT_PERC,drug.discountPerc);
        cv.put(COLUMN_TYPE,drug.type);
        cv.put(COLUMN_SLUG,drug.slug);

        cv.put(COLUMN_MRP,drug.mrp);
        cv.put(COLUMN_IMG_URL,drug.imgURL);
        cv.put(COLUMN_P_FORM,drug.pForm);
        cv.put(COLUMN_O_PRICE,drug.oPrice);

        cv.put(COLUMN_SU,drug.su);
        cv.put(COLUMN_PACK_SIZE,drug.packSize);

        return cv;

    }

}
