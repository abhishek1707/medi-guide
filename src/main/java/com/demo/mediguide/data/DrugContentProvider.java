package com.demo.mediguide.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by q4J1X056 on 01-12-2015.
 */
public class DrugContentProvider extends ContentProvider {

    // database
    private DrugDatabaseHelper database;

    // used for the UriMacher
    private static final int DRUGS = 10;
    private static final int DRUG_ID = 20;

    private static final String AUTHORITY = "com.demo.mediguide.data.drug.contentprovider";

    private static final String BASE_PATH = "drugs";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/drugs";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/drug";

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, DRUGS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", DRUG_ID);
    }

    @Override
    public boolean onCreate() {
        database = new DrugDatabaseHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        checkColumns(projection);

        // Set the table
        queryBuilder.setTables(Drug.TABLE_DRUG);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case DRUGS:
                break;
            case DRUG_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(Drug.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        long id = 0;
        switch (uriType) {
            case DRUGS:
                id = sqlDB.insert(Drug.TABLE_DRUG, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case DRUGS:
                rowsDeleted = sqlDB.delete(Drug.TABLE_DRUG, selection,
                        selectionArgs);
                break;
            case DRUG_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(Drug.TABLE_DRUG,
                            Drug.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(Drug.TABLE_DRUG,
                            Drug.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case DRUGS:
                rowsUpdated = sqlDB.update(Drug.TABLE_DRUG,
                        values,
                        selection,
                        selectionArgs);
                break;
            case DRUG_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(Drug.TABLE_DRUG,
                            values,
                            Drug.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(Drug.TABLE_DRUG,
                            values,
                            Drug.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = {
                Drug.COLUMN_ID,
                Drug.COLUMN_MFID,
                Drug.COLUMN_AVAILABLE,
                Drug.COLUMN_LABEL,
                Drug.COLUMN_MANUFACTURER,
                Drug.COLUMN_FORM,
                Drug.COLUMN_HKP_DRUG_CODE,
                Drug.COLUMN_U_PRICE,
                Drug.COLUMN_PACK_FORM,
                Drug.COLUMN_NAME,
                Drug.COLUMN_UIP,
                Drug.COLUMN_GENERICS,
                Drug.COLUMN_PRODUCTS_FOR_BRANDS,
                Drug.COLUMN_DISCOUNT_PERC,
                Drug.COLUMN_TYPE,
                Drug.COLUMN_SLUG,
                Drug.COLUMN_MRP,
                Drug.COLUMN_IMG_URL,
                Drug.COLUMN_P_FORM,
                Drug.COLUMN_O_PRICE,
                Drug.COLUMN_SU,
                Drug.COLUMN_PACK_SIZE
        };


        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }


    @Override
    public int bulkInsert(Uri uri, ContentValues[] values){
        int numInserted = 0;
        String table="";

        int uriType = sURIMatcher.match(uri);

        switch (uriType) {
            case DRUGS:
                table = Drug.TABLE_DRUG;
                break;
        }
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        sqlDB.beginTransaction();
        try {
            for (ContentValues cv : values) {
                long newID = sqlDB.insertOrThrow(table, null, cv);
                if (newID <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            sqlDB.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);
            numInserted = values.length;
        } finally {
            sqlDB.endTransaction();
        }
        return numInserted;
    }
}
