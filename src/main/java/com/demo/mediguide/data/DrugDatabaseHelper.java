package com.demo.mediguide.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by q4J1X056 on 01-12-2015.
 */
public class DrugDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "drug.db";
    private static final int DATABASE_VERSION = 2;

    public DrugDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        Drug.onCreate(database);
    }

    // Method is called during an upgrade of the database,
    // e.g. if you increase the database version
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {
        Drug.onUpgrade(database, oldVersion, newVersion);
    }

}
