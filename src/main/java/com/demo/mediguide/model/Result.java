package com.demo.mediguide.model;

import com.demo.mediguide.data.Drug;
import com.google.gson.annotations.SerializedName;

/**
 * Created by q4J1X056 on 01-12-2015.
 */
public class Result {

    @SerializedName("id")
    public int id;

    @SerializedName("mfId")
    public int mfId;

    @SerializedName("available")
    public boolean available;

    @SerializedName("label")
    public String label;

    @SerializedName("manufacturer")
    public String manufacturer;

    @SerializedName("form")
    public String form;


    @SerializedName("hkpDrugCode")
    public int hkpDrugCode;


    @SerializedName("uPrice")
    public double uPrice;

    @SerializedName("packForm")
    public String packForm;


    @SerializedName("name")
    public String name;

    @SerializedName("uip")
    public int uip;

    @SerializedName("generics")
    public String generics;

    @SerializedName("productsForBrand")
    public String productsForBrand;

    @SerializedName("discountPerc")
    public int discountPerc;

    @SerializedName("type")
    public String type;

    @SerializedName("slug")
    public String slug;

    @SerializedName("mrp")
    public double mrp;

    @SerializedName("imgURL")
    public String imgURL;

    @SerializedName("pForm")
    public String pForm;

    @SerializedName("oPrice")
    public double oPrice;

    @SerializedName("su")
    public int su;

    @SerializedName("packSize")
    public String packSize;



}
