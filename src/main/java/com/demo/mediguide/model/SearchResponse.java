package com.demo.mediguide.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by q4J1X056 on 01-12-2015.
 */
public class SearchResponse {

    @SerializedName("message")
    public String message;

    @SerializedName("op")
    public String op;


    @SerializedName("status")
    public int status;

    @SerializedName("totalRecordCount")
    public int totalRecordCount;

    @SerializedName("hasMore")
    public boolean hasMore;

    @SerializedName("suggestions")
    public String[] suggestions;


    public List<Result> result;

    // And more if Required



}
