package com.demo.mediguide.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.mediguide.data.Drug;
import com.demo.mediguide.data.DrugContentProvider;
import com.demo.mediguide.model.Result;
import com.demo.mediguide.model.SearchResponse;
import com.demo.mediguide.util.Constants;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

/**
 * Created by q4J1X056 on 01-12-2015.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    ContentResolver mContentResolver;
    Context mContext;
    public static final String SYNC_MESSAGE="sync_medi_guide";
    public static final String SYNC_EVENT="event";
    public static final int SYNC_START=0;
    public static final int SYNC_END=1;
    public static final int BATCH_SIZE=50;
    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContext=context;
        mContentResolver = context.getContentResolver();
    }
    //...
    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContext=context;
        mContentResolver = context.getContentResolver();
    }



    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {
        sendLocalBroadcast(SYNC_START);

        SyncResult result = new SyncResult();
        try {
            deleteDrug(provider);
            insertDrugs(provider);
        } catch (  Exception e) {
            syncResult.hasHardError();
            sendLocalBroadcast(SYNC_END);
        }


    }



    private void deleteDrug(ContentProviderClient contentProviderClient)
            throws Exception {


                contentProviderClient.delete(
                        DrugContentProvider.CONTENT_URI,
                        null, null);


    }




    private void insertDrugs(final ContentProviderClient contentProviderClient)
            throws RemoteException
    {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.SERVER_PATH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                        Gson gson = new Gson();
                        SearchResponse searchResponse=gson.fromJson(response, SearchResponse.class);
                        List<Result> resultList=searchResponse.result;
                        if(resultList!=null && resultList.size()>0) {
                            ContentValues[] contentValuesArray;
                            int i=0;
                            while(i<resultList.size())
                            {
                                if(resultList.size()-i<BATCH_SIZE) {
                                    contentValuesArray = new ContentValues[resultList.size() - i];
                                }
                                else
                                {
                                    contentValuesArray = new ContentValues[BATCH_SIZE];
                                }

                                for(int j=0;j< contentValuesArray.length;j++)
                                {
                                    contentValuesArray[j]=Drug.getContentValuesFromModel(resultList.get(i++));
                                }


                                    try {
                                        contentProviderClient.bulkInsert(DrugContentProvider.CONTENT_URI, contentValuesArray);
                                    }
                                    catch (RemoteException ex)
                                    {

                                    }





                            }



                        }
                         else
                        {
                            Toast.makeText(mContext,"No data found!",Toast.LENGTH_SHORT).show();
                            //sendLocalBroadcast(SYNC_END);
                        }
                        } catch (Exception e) {

                        }
                        finally {
                            sendLocalBroadcast(SYNC_END);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.getMessage());
                error.printStackTrace();
                Toast.makeText(mContext,"Oops! Error fetching the data",Toast.LENGTH_SHORT).show();
                sendLocalBroadcast(SYNC_END);
            }
        });
// Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);

    }



    void sendLocalBroadcast(int eventType)
    {
        Intent intent = new Intent(SYNC_MESSAGE);
        intent.putExtra(SYNC_EVENT, eventType);
        getContext().getApplicationContext().sendBroadcast(intent);
        System.out.println("Broadcast"+ eventType);

    }







}
