package com.demo.mediguide.adapters;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.demo.mediguide.data.Drug;
import com.demo.mediguide.fragments.DrugDetailFragment;

/**
 * Created by q4J1X056 on 02-12-2015.
 */
public class DrugDetailsFragmentAdapter  extends FragmentStatePagerAdapter {

    private Cursor mCursor;

    public DrugDetailsFragmentAdapter(FragmentManager fm, Cursor c) {
        super(fm);
        mCursor = c;
    }

    @Override
    public Fragment getItem(int position) {
        if(mCursor==null)
            return null;
        if (mCursor.moveToPosition(position)) {
            Bundle arguments = new Bundle();
            arguments.putInt(DrugDetailFragment.ARG_ITEM_ID, mCursor.getInt(mCursor.getColumnIndex(Drug.COLUMN_ID)));
            DrugDetailFragment fragment = new DrugDetailFragment();
            fragment.setArguments(arguments);
            return fragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        if (mCursor != null) {

            return mCursor.getCount();

        }
        return 0;
    }

    public void swapCursor(Cursor cursor) {
        if (mCursor == cursor)
            return;

        mCursor = cursor;
        notifyDataSetChanged();

    }

}
