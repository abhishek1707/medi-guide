package com.demo.mediguide;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;

import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.mediguide.adapters.DrugDetailsFragmentAdapter;
import com.demo.mediguide.data.DrugContentProvider;
import com.demo.mediguide.model.Result;
import com.demo.mediguide.model.SearchResponse;
import com.demo.mediguide.sync.SyncAdapter;
import com.demo.mediguide.util.Constants;
import com.google.gson.Gson;

import java.io.InputStream;
import java.util.List;


public class MainActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    DrugDetailsFragmentAdapter adapter;
    private ViewPager mPager;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent

            int eventType = intent.getIntExtra(SyncAdapter.SYNC_EVENT,1);
            if(eventType==SyncAdapter.SYNC_START) {
                setProgressBarIndeterminateVisibility(true);

            }
            else {
                setProgressBarIndeterminateVisibility(false);

            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);
        mPager = (ViewPager) findViewById(R.id.pager);



        registerReceiver(mMessageReceiver,
                new IntentFilter(SyncAdapter.SYNC_MESSAGE));

        // In this we can insert the logic whether to do the sync or not
        performSync();

        getLoaderManager().initLoader(1, null, this);






    }

    @Override
    protected void onStart() {
        super.onStart();

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_sync) {
//            performSync();
//
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {

        return new CursorLoader(this, DrugContentProvider.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {

        if(cursor.getCount()>0) {
            if (adapter == null) {
                adapter = new DrugDetailsFragmentAdapter(getSupportFragmentManager(), null);
                mPager.setAdapter(adapter);
            } else
                adapter.swapCursor(cursor);
        }




    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // If the Cursor is being placed in a CursorAdapter, you should use the
        // swapCursor(null) method to remove any references it has to the
        // Loader's data.
        if(adapter!=null)
            adapter.swapCursor(null);

    }



    void performSync()
    {
        Account newAccount = new Account("dummyaccount", "example.com");
        AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);

        accountManager.addAccountExplicitly(newAccount, null, null);
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(
                newAccount,"com.demo.mediguide.data.drug.contentprovider", settingsBundle);
    }


    @Override
    protected void onStop() {

        if(mMessageReceiver!=null)
        {
            try {
                unregisterReceiver(mMessageReceiver);
            }catch (Exception ex)
            {

            }

        }
            super.onStop();
    }
}
