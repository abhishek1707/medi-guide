package com.demo.mediguide;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;


public class SplashActivity extends Activity {

    boolean isDestroyed=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        ImageView splashImageView = (ImageView) findViewById(R.id.imageViewSplash);
        splashImageView.setBackgroundResource(R.drawable.splash_screen);
        AnimationDrawable frameAnimation = (AnimationDrawable)splashImageView.getBackground();


        frameAnimation.start();


        Thread mSplashThread =  new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {

                        wait(5000);
                    }
                } catch (InterruptedException ex) {
                }



                // Run next activity
                if(!isDestroyed)
                {
                    Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                }

                finish();

                //stop();
            }};
        mSplashThread.start();



    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isDestroyed=true;
    }
}
