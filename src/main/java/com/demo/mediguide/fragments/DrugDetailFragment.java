package com.demo.mediguide.fragments;


import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demo.mediguide.R;
import com.demo.mediguide.data.Drug;
import com.demo.mediguide.data.DrugContentProvider;

/**
 * Created by q4J1X056 on 02-12-2015.
 */
public class DrugDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID="id";


    int id;
    public DrugDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            id = getArguments().getInt(ARG_ITEM_ID);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_drug_detail, container, false);

        TextView textViewName=(TextView)rootView.findViewById(R.id.textViewName);
        TextView textViewLabel=(TextView)rootView.findViewById(R.id.textViewlabel);
        TextView textViewMfr=(TextView)rootView.findViewById(R.id.textViewMfr);
        TextView textViewUnitPrice=(TextView)rootView.findViewById(R.id.textViewUnitPrice);


        // Show the dummy content as text in a TextView.
        if (id > 0) {
            Uri contentUri = ContentUris.withAppendedId(DrugContentProvider.CONTENT_URI, id);
            Cursor c = getActivity().getContentResolver().query(contentUri, null, null, null, null);

            if (c != null) {
                if (c.moveToFirst()) {
                    String name = c.getString(c.getColumnIndex(Drug.COLUMN_NAME));
                    String label= c.getString(c.getColumnIndex(Drug.COLUMN_LABEL));
                    String mfr=c.getString(c.getColumnIndex(Drug.COLUMN_MANUFACTURER));
                    String uPrice= c.getString(c.getColumnIndex(Drug.COLUMN_U_PRICE));

                    textViewName.setText(name);
                    textViewLabel.setText(label);
                    textViewMfr.setText(mfr);
                    textViewUnitPrice.setText("INR "+ uPrice);


                }
                c.close();
            }
        }
        else
        return null;

        return rootView;
    }

}
